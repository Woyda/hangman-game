var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	babel = require('gulp-babel'),
	concat = require('gulp-concat'),
	clean = require('gulp-clean'),
	runSequence = require('run-sequence'),
	browserSync = require('browser-sync').create();

//clean dist
gulp.task('clean-dist', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

// Sass - compiles SCSS and prefixes CSS and streams CSS to browserSync
gulp.task('sass', function() {

	return gulp.src('src/scss/*.scss')

		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({browsers: ['last 5 versions', '> 1%', 'IE 9']}))
		.pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream()); 

});

// Copy - index.html
gulp.task('copy:html', function() {

	return gulp.src('src/index.html')
		.pipe(gulp.dest('dist'));

});

//Babel - transpile JS
gulp.task('babel', function() {
	gulp.src('src/js/*.js')
		.pipe(babel({
			presets: ['env']
		}))
		.pipe(concat('app.js'))
		.pipe(gulp.dest('dist/js'))
		.pipe(browserSync.stream()); 
});

// Watch
gulp.task('watch', function() {

	gulp.watch('src/scss/**/*.scss', ['sass']);
	gulp.watch('src/index.html', ['copy:html']);
	gulp.watch('src/js/*.js', ['babel']);

});


// BrowserSync
gulp.task('serve', function() {

    browserSync.init({
        server: "./dist"
    });

    gulp.watch("src/**/*.html").on('change', browserSync.reload);
});

// Copy - images - Copies images
gulp.task('copy:images', function() {

	return gulp.src('src/img/**/*.+(png|jpg|gif|svg)')
		.pipe(gulp.dest('dist/img'));

});

// Copy - fonts
gulp.task('copy:fonts', function() {

	return gulp.src('src/fonts/*')
		.pipe(gulp.dest('dist/fonts'));

});

// Build
gulp.task('build', function() {

	runSequence('copy:images', 'copy:fonts');

});


// Default
gulp.task('default', function() {

	runSequence('clean-dist', 'sass', 'babel', 'watch', 'copy:html', 'serve', 'build');

});

