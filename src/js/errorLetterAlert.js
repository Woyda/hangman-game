//Initiated inside checkLetterError function
function errorLetterAlert(e) {
	let wrongLetters = document.querySelectorAll('.wrong-letter');
	let keyChar = e.key.toLowerCase();

	wrongLetters.forEach((el) => {

		if (el.innerHTML === keyChar) {
			el.classList.add('duplicate-choice');

			setTimeout(() => {
				el.classList.remove('duplicate-choice');
			}, 300);
		}
	});
}