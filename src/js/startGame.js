function startGame() {
	const apiUrl = 'http://api.wordnik.com:80/v4/words.json/randomWord?hasDictionaryDef=false&minCorpusCount=0&maxCorpusCount=-1&minDictionaryCount=1&maxDictionaryCount=-1&minLength=3&maxLength=11&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5';

	let xml = new XMLHttpRequest();

	xml.open('GET', apiUrl, true);
	xml.onreadystatechange = () => {

		if ( xml.readyState == 4 ) {
			const wordMaxLength = 11;

			let parsed = JSON.parse(xml.responseText);
			let word = [...parsed.word];
			let wordLength = word.length;
			let unactiveAmout = wordMaxLength - wordLength;
			let letters = document.querySelectorAll('.letter-box');

			word = word.map((el) => {
				return el.toLowerCase();
			});

			letters.forEach((el, index) => {
				if (index > unactiveAmout - 1 ) {
					el.classList.add('active-box');
				}
			});

			const activeLetters = document.querySelectorAll('.active-box');

			activeLetters.forEach((el, index) => {

				if(word[index] === ' ' || word[index] === '-') {
					el.classList.remove('active-box');
				} else {
					el.innerHTML = word[index];
				}
			});

			xml = null;
		}
	};
	xml.send();
}