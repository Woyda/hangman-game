function checkLetterCorrect(e) {
	const gameOver = document.querySelector('.game-over');

	if (!gameOver) {
		const activeLetters = [...document.querySelectorAll('.active-box')];

		let keyChar = e.key.toLowerCase();

		activeLetters.forEach((el) => {
			if (el.innerHTML === keyChar) {
				el.classList.add('correct');
			} else {
				return false;
			}
		});
	}
}