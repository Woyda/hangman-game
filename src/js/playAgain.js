function playAgain(e) {
	let gameOver = document.querySelector('.game-over');
	let resetBtn = document.getElementById('play-again-btn');

	if (gameOver && (e.keyCode === 13 || e.target == resetBtn)) {
		let letterBox = [...document.querySelectorAll('.letter-box')];
		let mistakesWrapper = document.querySelector('.wrong-letters-wrapper');
		let hangmanElement = [...document.querySelectorAll('.hangman-element')];

		let gameWrapper = document.querySelector('.game-wrapper');

		letterBox.forEach((el) => {
			el.innerHTML = '';
			el.classList.remove('active-box');
			el.classList.remove('correct');
		});

		hangmanElement.forEach((el) => {
			if (!el.classList.contains('hide')) {
				el.classList.add('hide');
			}
		});

		mistakesWrapper.innerHTML = '';
		gameWrapper.classList.remove('game-over');

		startGame();
	}
}