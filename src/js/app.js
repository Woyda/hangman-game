// Init functions
startGame();
document.addEventListener('keyup', function(e) {
	if (e.keyCode >= 65 && e.keyCode <= 90) {
		checkLetterCorrect(e);
		checkLetterError(e);
		hangmanFlow(e)
		gameOver(e);
	}
});
document.addEventListener('keyup', playAgain, false);
document.getElementById('play-again-btn').addEventListener('click', playAgain, false);


