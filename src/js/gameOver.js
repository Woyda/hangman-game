function gameOver(e) {
	const maxMistakes = 11;
	let amountWrongLetters = [...document.querySelectorAll('.wrong-letter')].length;
	let amountActiveLetters = [...document.querySelectorAll('.active-box')].length;
	let amoutCorrectLetters = [...document.querySelectorAll('.correct')].length;
	let gameWrapper = document.querySelector('.game-wrapper');

	if (amountWrongLetters >= maxMistakes || amoutCorrectLetters >= amountActiveLetters) {
		gameWrapper.classList.add('game-over');
	}
}