function checkLetterError(e) {
	const gameOver = document.querySelector('.game-over');

	if(!gameOver) {
		const activeLetters = [...document.querySelectorAll('.active-box')];

		let wrongLettersContainer = document.querySelector('.wrong-letters-wrapper');
		let wrongLetter = document.createElement('span');
		let controlNo = 0;
		let keyChar = e.key.toLowerCase();

		let wrongLettersArray = [...document.querySelectorAll('.wrong-letter')];
		let wrongLetters = wrongLettersArray.map((el) => {
			return el.innerHTML;
		});

		activeLetters.forEach((el) => {

			if (el.innerHTML !== keyChar) {
				controlNo++;

				if (controlNo === activeLetters.length && !wrongLetters.includes(keyChar)) {

					wrongLetter.classList.add('wrong-letter');
					wrongLetter.innerHTML = keyChar;
					wrongLettersContainer.appendChild(wrongLetter);
				} else {
					errorLetterAlert(e);
				}
			}
		});
	}
}