function hangmanFlow(e) {
	let amountWrongLetters = [...document.querySelectorAll('.wrong-letter')].length;
	const maxMistakes = 11;
	let hangmanElement = document.querySelector(`[data-mistake-no='${amountWrongLetters}']`);

	if(amountWrongLetters <= maxMistakes) {
		hangmanElement.classList.remove('hide');
	}
}